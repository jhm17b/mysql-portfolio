### :floppy_disk: All recovered previous MySQL Coursework Portfolio :floppy_disk:

## :space_invader: Jack H. Miller :space_invader:
**************************************************************************************************************************************************
I have been battling a vigourous fight against an agressive form of Crohn's while working towards my degree, this battle has consisted of multiple surgeries including a life changing colostomy surgery that I underwent and immediately had final exams and projects to take and submit. From previous experience with working through my strenious courses while dealing with a debilating disease I am able to adapt and keep up with the projects presented to me.

I am currently an IT Senior graduating at Florida State University in Spring 2022, pursuing my technology related passions through a Bachelor's degree in Information Technology while fighting an aggressive form of Crohn's disease.
This passion for technology and knowledge being so great that even immediately coming to from a colostomy surgery cannot prevent me from taking summer finals to further my degree and the pursuit of growing my technological based skills and interests.
However, outside of academics you can find me enjoying some of my hobbies, which include things like evening runs, camping trips with friends, collecting the vinyl's for my favorite albums, or just relaxing after a long day with some video games or a nice book.
**********************************************************************************************************************************************
### :computer: LIS Intermediate Database Management Coursework :computer:
This is a recovered portfolio of coursework during my time at FSU, showcasing an understanding utilizing MySQL and Microsoft SQL Server.
***********************************************************************************************************************************************
## Screenshots:

### A2 University StudentFaculty Database
![a2ERD.png](imgs/a2ERD.png)

*************************************************************************************************************************************************

### A3 Library Database
![a3LibraryERD.png](imgs/a3LibraryERD.png)

************************************************************************************************************************************************

### A5 Microsoft SQL Server Real Estate Properties Database
![a5Properties.png](imgs/a5PropertiesERD.png)

*******************************************************************************************************************************************************

### P1 Banking Database
![p1BankingERD.png](imgs/P1BankingERD.png)

*******************************************************************************************************************************************************

### P2 Microsoft SQL Server Electronic Medical Record Database
![p2MedicalRecordDatabase.png](imgs/P2MedicalRecordERD.png)

![p2Tables.png](imgs/P2Tables.png)

![p2TablesCont.png](imgs/P2TablesCont.png)

*******************************************************************************************************************************************************

## :floppy_disk: Database Concepts Projects :floppy_disk:

### P1 Generic Sport Team Database
![DCP1SportTeamDatabaseERD.png](imgs/SportTeamERD.png)


### P2 Company Database
![DCP2CompanyDatabaseERD.png](imgs/CompanyERD.png)