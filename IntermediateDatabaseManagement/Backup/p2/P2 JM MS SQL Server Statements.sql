--Project 2 MS SQL Server Statements **Bank Data**
--Start up procedure--
SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jhm17b')
DROP DATABASE jhm17b;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jhm17b')
CREATE DATABASE jhm17b;
GO

use jhm17b;
GO

IF OBJECT_ID (N'dbo.patient', N'U') IS NOT NULL
DROP TABLE dbo.patient;
GO

CREATE TABLE dbo.patient
(
    pat_id SMALLINT not null identity(1,1),
    pat_ssn INT not null check (pat_ssn > 0 and pat_ssn <= 999999999),
    pat_fname VARCHAR(15) not null,
    pat_lname VARCHAR(30) not null,
    pat_street VARCHAR(30) not null,
    pat_city VARCHAR(30) not null,
    pat_state CHAR(2) not null DEFAULT 'FL',
    pat_zip INT not null check (pat_zip > 0 and pat_zip <= 999999999),
    pat_phone BIGINT not null check (pat_phone > 0 and pat_phone <= 9999999999),
    pat_email VARCHAR(100) null,
    pat_dob DATE not null,
    pat_gender CHAR(1) not null check (pat_gender IN ('m','f')),
    pat_notes VARCHAR(255) null,
    PRIMARY KEY(pat_id),

    CONSTRAINT ux_pat_ssn unique nonclustered (pat_ssn ASC)
);

IF OBJECT_ID (N'dbo.medication',N'U') IS NOT NULL
DROP TABLE dbo.medication;

CREATE TABLE dbo.medication
(
    med_id SMALLINT NOT NULL identity(1,1),
    med_name VARCHAR(100) NOT NULL,
    med_price DECIMAL(5,2) NOT NULL CHECK (med_price > 0),
    med_shelf_life DATE NOT NULL,
    med_notes VARCHAR(255) NULL,
    PRIMARY KEY (med_id)
);

IF OBJECT_ID (N'dbo.prescription', N'U') IS NOT NULL
DROP TABLE dbo.prescription;

CREATE TABLE dbo.prescription
(
    pre_id SMALLINT not null identity(1,1),
    pat_id SMALLINT not null,
    med_id SMALLINT not null,
    pre_date DATE not null,
    pre_dosage VARCHAR(255) not null,
    pre_num_refills VARCHAR(3) not null,
    pre_notes VARCHAR(255) null,
    PRIMARY KEY (pre_id),

    CONSTRAINT ux_pat_id_med_id_pre_date unique nonclustered
    (pat_id ASC, med_id ASC, pre_date ASC),

    CONSTRAINT fk_prescription_patiet
    FOREIGN KEY (pat_id)
    REFERENCES dbo.patient (pat_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

    CONSTRAINT fk_prescription_medication
    FOREIGN KEY (med_id)
    REFERENCES dbo.medication (med_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.treatment',N'U') IS NOT NULL
DROP TABLE dbo.treatment;

CREATE TABLE dbo.treatment
(
    trt_id SMALLINT NOT NULL identity(1,1),
    trt_name VARCHAR(255) NOT NULL,
    trt_price DECIMAL(8,2) NOT NULL check (trt_price > 0),
    trt_notes VARCHAR(255) NULL,
    PRIMARY KEY (trt_id)
);

IF OBJECT_ID(N'dbo.physician',N'U') IS NOT NULL
DROP TABLE dbo.physician;
GO

CREATE TABLE dbo.physician
(
    phy_id SMALLINT not null identity(1,1),
    phy_specialty VARCHAR(25) NOT NULL,
    phy_fname VARCHAR(15) NOT NULL,
    phy_lname VARCHAR(30) NOT NULL,
    phy_street VARCHAR(30) NOT NULL,
    phy_city VARCHAR(30) NOT NULL,
    phy_state CHAR(2) NOT NULL DEFAULT 'FL',
    phy_zip int NOT NULL check (phy_zip > 0 and phy_zip <= 999999999),
    phy_phone BIGINT NOT NULL check (phy_phone > 0 and phy_phone <= 9999999999),
    phy_fax BIGINT NOT NULL check (phy_fax > 0 and phy_fax <= 9999999999),
    phy_email VARCHAR(100) NULL,
    phy_url VARCHAR(100) NULL,
    phy_notes VARCHAR(255) NULL,
    PRIMARY KEY (phy_id)
);

IF OBJECT_ID(N'dbo.patient_treatment',N'U') IS NOT NULL
DROP TABLE dbo.patient_treatment;

CREATE TABLE dbo.patient_treatment
(
    ptr_id SMALLINT NOT NULL identity(1,1),
    pat_id SMALLINT NOT NULL,
    phy_id SMALLINT NOT NULL,
    trt_id SMALLINT NOT NULL,
    ptr_date DATE NOT NULL,
    ptr_start TIME(0) NOT NULL,
    ptr_end TIME(0) NOT NULL,
    ptr_results VARCHAR(255) NULL,
    ptr_notes VARCHAR(255) NULL,
    PRIMARY KEY(ptr_id),

    CONSTRAINT ux_pat_id_phy_id_trt_id_ptr_date unique nonclustered
    (pat_id ASC, phy_id ASC, trt_id ASC, ptr_date ASC),

    CONSTRAINT fk_patient_treatment_patient
      FOREIGN KEY (pat_id)
      REFERENCES dbo.patient (pat_id)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,

    CONSTRAINT fk_patient_treatment_physician
      FOREIGN KEY(phy_id)
      REFERENCES dbo.physician(phy_id)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,

    CONSTRAINT fk_patient_treatment_treatment
      FOREIGN KEY (trt_id)
      REFERENCES dbo.treatment(trt_id)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.administration_lu',N'U') IS NOT NULL
DROP TABLE dbo.administration_lu;

CREATE TABLE dbo.administration_lu
(
    pre_id SMALLINT NOT NULL,
    ptr_id SMALLINT NOT NULL,
    PRIMARY KEY(pre_id, ptr_id),

    --either one of these FKs may be ON DELETE/ON UDPATE CASCADE, but *NOT* both!
    --if both FKs containt CASCADE, generates "may cause cycles or multiple cascade paths" error!
    CONSTRAINT fk_administration_lu_prescription
      FOREIGN KEY (pre_id)
      REFERENCES dbo.prescription (pre_id)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,

    CONSTRAINT fk_administration_lu_patient_treatment
      FOREIGN KEY (ptr_id)
      REFERENCES dbo.patient_treatment (ptr_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

--Show tables
SELECT * FROM information_schema.tables;

--disble all constraints (must do this *after* table creation, but *before* inserts)
EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"


--Data for table dbo.patient
INSERT INTO dbo.patient
(pat_ssn, pat_fname, pat_lname, pat_street, pat_city, pat_state, pat_zip, pat_phone, pat_email, pat_dob, pat_gender, pat_notes)

--notice:SQL server accepts dates in various formats

VALUES
('123456789','Carla','Vanderbilt','5133 3rd road','Lake Worth','FL','334671234',5674892390,'csweeny@yahoo.com','11-26-1961','F',NULL),
('456789012','Jake','Ryan','4200 Apple Drive','Sarasota','FL','323410090',091214300,'JRyan@yahoo.com','12-30-1970','M',NULL),
('789012345','Mario','Bro','3450 Forest Lane','Tallahassee','FL','450121987',9418889001,'MBro@yahoo.com','01-06-1980','M',NULL),
('012345678','Luigi','Bro','3330 New Lake Road','Fort Myers','FL','909020334',8092213450,'LBro@yahoo.com','02-16-1990','M',NULL),
('890123456','Georgie','Lopez','5000 Sunset Lake Drive','Lake Worth','FL','323405666',9418093400,'GLopez@yahoo.com','04-26-1961','M',NULL);

INSERT INTO dbo.medication
(med_name, med_price, med_shelf_life, med_notes)

VALUES
('Abilify',200.00,'06-23-2014',NULL),
('Aciphex',125.00,'06-24-2014',NULL),
('Actonel',250.00,'06-25-2014',NULL),
('Actoplus MET',412.00,'06-26-2014',NULL),
('Actos',89.00,'06-25-2014',NULL);

INSERT INTO dbo.physician
(phy_specialty, phy_fname, phy_lname, phy_street, phy_city, phy_state, phy_zip, phy_phone, phy_fax, phy_email, phy_url, phy_notes)

VALUES
('family medicine','tom','smith','987 peach street','tampa','FL','33610','987654125','9998854545','tsmith@gmail.com','tsmithfamilymed.com',NULL),
('internal medicine','steve','williams','963 plum lane','miami','FL','33610','6001004545','8887006543','swilliams@gmail.com','swilliamsmedicine.com',NULL),
('pediatrician','ronald','burns','645 wave circle','orlando','FL','33450','7105419090','7771213490','rburns@gmail.com','rburnspediatrics.com',NULL),
('psychiatrist','pete','rogers','1233 stadium lane','orlando','FL','33200','8007509010','6665094412','progers@gmail.com','progerpsych.com',NULL),
('dermatologist','dave','rogers','687 hard drive','miami','FL','34510','9416007979','9091237654','drogers@gmail.com','drogerderma.com',NULL);

INSERT INTO dbo.prescription
(pat_id, med_id, pre_date, pre_dosage, pre_num_refills, pre_notes)

VALUES
(1,1,'2011-12-23','take one per day','1',NULL),
(2,2,'2011-12-24','take as needed','2',NULL),
(2,3,'2011-12-25','take two before and after dinner','1',NULL),
(2,4,'2011-12-26','take one per day','2',NULL),
(3,5,'2011-12-27','take as needed','1',NULL);

INSERT INTO dbo.treatment
(trt_name, trt_price, trt_notes)

VALUES
('knee replacement',2000.00,NULL),
('heart transplant',130000.00,NULL),
('hip replacement',40000.00,NULL),
('tonsils removed',5000.00,NULL),
('skin graft', 2000.00,NULL);

--SET IDENTITY_INSERT patient_treatment on
INSERT INTO patient_treatment
(pat_id,phy_id,trt_id,ptr_date,ptr_start,ptr_end,ptr_results,ptr_notes)

VALUES
(1,1,5,'2011-12-23','07:08:09','10:12:15','success patient is fine',NULL),
(1,2,1,'2011-12-24','08:08:09','11:12:15','complications patient will repeat procedure at a later time',NULL),
(2,3,4,'2011-12-25','09:08:09','12:12:15','died during surgery',NULL),
(5,2,2,'2011-12-26','10:08:09','13:12:15','success patient is fine',NULL),
(2,3,4,'2011-12-27','11:08:09','14:12:15','complications patient will repeat procedure at a later time',NULL);
--SET IDENTITY_INSERT patient_treatment off

INSERT INTO dbo.administration_lu
(pre_id, ptr_id)

VALUES (1,5),(2,3),(4,4),(5,2),(2,4);

--enable all restraints
EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

--Statements MS SQL Server
Use jhm17b;
GO

BEGIN transaction
  SELECT pat_fname, pat_lname, pat_notes, med_name, med_price, med_shelf_life, pre_dosage, pre_num_refills
  from medication m
    join prescription pr on pr.med_id = m.med_id
    join patient p on pr.pat_id = p.pat_id
  order by med_price desc;
commit;
--B.
IF OBJECT_ID (N'dbo.v_physician_treatments',N'V') IS NOT NULL
DROP VIEW dbo.v_physician_patient_treatments;
GO

create view dbo.v_physician_patient_treatments as
select phy_fname, phy_lname, trt_name, trt_price, ptr_results, ptr_date, ptr_start, ptr_end
from physician p, patient_treatment pt, treatment t
where p.phy_id=pt.phy_id
and pt.trt_id=t.trt_id
go

select * from dbo.v_physician_patient_treatments order by trt_price desc;
go
--C.Create a stored procedure (Add Record) that adds the following record to the patient treatment table
select * from dbo.v_physician_patient_treatments;

IF OBJECT_ID('AddRecord') IS NOT NULL
DROP PROCEDURE AddRecord;
GO

CREATE PROCEDURE AddRecord AS
  insert into dbo.patient_treatment
  (pat_id, phy_id, trt_id, ptr_date, ptr_start, ptr_end, ptr_results,ptr_notes)
  values (5,5,5,'2013-04-23','11:00:00','12:30:00','released','ok');

select * from dbo.v_physician_patient_treatments;
go

EXEC AddRecord;
--D.
BEGIN TRANSACTION
select * from dbo.administration_lu;
delete from dbo.administration_lu where pre_id = 5 and ptr_id = 2;
select * from dbo.administration_lu;
commit;
--E.Create a stored procedure (UpdateRecord) to update the patients's last name to "Vanderbilt" whos id is 3
IF OBJECT_ID('dbo.UpdatePatient') IS NOT NULL
DROP PROCEDURE dbo.UpdatePatient;
GO

CREATE PROCEDURE dbo.UpdatePatient AS

--check data before
select * from dbo.patient;

update dbo.patient
set pat_lname = 'Vanderbilt'
where pat_id = 3;

--check data after
select * from dbo.patient;
go

EXEC dbo.UpdatePatient;

--F.
EXEC sp_help 'dbo.patient_treatment';
ALTER TABLE dbo.patient_treatment add ptr_prognosis varchar(255) NULL default 'testing';
EXEC sp_help 'dbo.patient_treatment';
