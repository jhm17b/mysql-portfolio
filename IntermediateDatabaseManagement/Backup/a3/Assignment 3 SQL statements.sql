--Assignment 3 query statements and answers

--Generic Syntax (one-based position):
SUBSTRING(string, position, length)

--Examples
select substring(850)

-- demo format phone data...
select mem_id, concat(mem_lname, ',', mem_fname) as name,
CONCAT('(', substring(mem_phone,1,3),')', substring(mem_phone,4,3),'-', substring(mem_phone,7,4)) as mem_phone,
 mem_email
 from member
 order by mem_lname asc;

--3. Create a stored derived attribute based upon the calculation above for the second book in the book table
-- and place the results in member #3's notes attribute

select * from member;

--step 1.
select bok_price, bok_price * .85
from book
where bok_isbn='1234567890345';

--step 2.
select concat('Purchased book at discounted price: ', '$',format(bok_price * .85,2))
from book
where bok_isbn='1234567890345';

--step 3.
--check data
select * from member;

update member
set mem_notes =
  (
      select concat('Purchased book at discounted price: ', '$',format(bok_price * .85,2))
      from book
      where bok_isbn='1234567890345';
  )
where mem_id = 3;