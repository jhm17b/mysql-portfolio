--Assignment 5 MS SQL Server Statements related to property management
SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jhm17b')
DROP DATABASE jhm17b;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jhm17b')
CREATE DATABASE jhm17b;

use jhm17b;
GO
--Table dbo.applicant
--drop table if exists
--N=subsequent string may be in Unicode (makes it portable to use with Unicode characters)
--U=only look for objects with this name that are tables
--be sure to sure dbo. before *all* table references
IF OBJECT_ID (N'dbo.applicant', N'U') IS NOT NULL
DROP TABLE dbo.applicant;
GO

CREATE TABLE dbo.applicant
(
    app_id SMALLINT not null identity(1,1),
    app_ssn int NOT NULL check (app_ssn > 0 and app_ssn <= 999999999),
    app_state_id VARCHAR(45) NOT NULL,
    app_fname VARCHAR(15) NOT NULL,
    app_lname VARCHAR(30) NOT NULL,
    app_street VARCHAR(30) NOT NULL,
    app_city VARCHAR(30) NOT NULL,
    app_state CHAR(2) NOT NULL DEFAULT 'FL',
    app_zip int NOT NULL check (app_zip > 0 and app_zip <= 999999999),
    app_email VARCHAR(100) NULL,
    app_dob DATE NOT NULL,
    app_gender CHAR(1) NOT NULL CHECK (app_gender IN('m', 'f')),
    app_bckgd_check CHAR(1) NOT NULL CHECK (app_bckgd_check IN('n', 'y')),
    app_notes VARCHAR(45) NULL,
    PRIMARY KEY (app_id),

    CONSTRAINT ux_app_ssn unique nonclustered (app_ssn ASC),
    CONSTRAINT ux_app_state_id unique nonclustered(app_state_id ASC)

);

IF OBJECT_ID (N'dbo.property', N'U') IS NOT NULL
DROP TABLE dbo.property;

CREATE TABLE dbo.property(
    prp_id SMALLINT NOT NULL identity(1,1),
    prp_street VARCHAR(30) NOT NULL,
    prp_city VARCHAR(30) NOT NULL,
    prp_state CHAR(2) NOT NULL DEFAULT 'FL',
    prp_zip int NOT NULL check (prp_zip > 0 and prp_zip <=999999999),
    prp_type varchar(15) NOT NULL CHECK (prp_type IN('house','condo','townhouse','duplex','apt','mobile home','room')),
    prp_rental_rate DECIMAL(7,2) NOT NULL CHECK (prp_rental_rate > 0),
    prp_status CHAR(1) NOT NULL CHECK (prp_status IN('a','u')),
    prp_notes VARCHAR(255) NULL,
    PRIMARY KEY (prp_id)
);

IF OBJECT_ID (N'dbo.agreement', N'U') IS NOT NULL
DROP TABLE dbo.agreement;

CREATE TABLE dbo.agreement(
    agr_id SMALLINT NOT NULL identity(1,1),
    prp_id SMALLINT NOT NULL,
    app_id SMALLINT NOT NULL,
    agr_signed DATE NOT NULL,
    agr_start DATE NOT NULL,
    agr_end DATE NOT NULL,
    agr_amt DECIMAL(7,2) NOT NULL CHECK (agr_amt > 0),
    agr_notes VARCHAR(255) NULL,
    PRIMARY KEY (agr_id),
    CONSTRAINT ux_prp_id_agr_signed unique nonclustered
    (prp_id ASC, app_id ASC, agr_signed ASC),

    CONSTRAINT fk_agreement_property
    FOREIGN KEY (prp_id)
    REFERENCES dbo.property (prp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_agreement_applicant
    FOREIGN KEY (app_id)
    REFERENCES dbo.applicant (app_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.feature', N'U') IS NOT NULL
DROP TABLE dbo.feature;

CREATE TABLE dbo.feature
(
    ftr_id TINYINT NOT NULL identity(1,1),
    ftr_type VARCHAR(45) NOT NULL,
    ftr_notes VARCHAR(255) NULL,
    PRIMARY KEY (ftr_id)
);

IF OBJECT_ID (N'dbo.prop_feature', N'U') IS NOT NULL
DROP TABLE dbo.prop_feature;

CREATE TABLE dbo.prop_feature
(
    pft_id SMALLINT NOT NULL identity(1,1),
    prp_id SMALLINT NOT NULL,
    ftr_id TINYINT NOT NULL,
    pft_notes VARCHAR(255) NULL,
    PRIMARY KEY (pft_id),

    CONSTRAINT ux_prp_id__ftr_id unique nonclustered (prp_id ASC, ftr_id ASC),

    CONSTRAINT fk_prop_feat_property
    FOREIGN KEY (prp_id)
    REFERENCES dbo.property (prp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_prop_feat_feature
    FOREIGN KEY (ftr_id)
    REFERENCES dbo.feature (ftr_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.occupant', N'U') IS NOT NULL
DROP TABLE dbo.occupant;

CREATE TABLE dbo.occupant
(
    ocp_id SMALLINT NOT NULL identity(1,1),
    app_id SMALLINT NOT NULL,
    ocp_ssn int NOT NULL check (ocp_ssn > 0 and ocp_ssn <= 999999999),
    ocp_state_id VARCHAR(45) NULL,
    ocp_fname VARCHAR(15) NOT NULL,
    ocp_lname VARCHAR(30) NOT NULL,
    ocp_email VARCHAR(100) NULL,
    ocp_dob DATE NOT NULL,
    ocp_gender CHAR(1) NOT NULL CHECK (ocp_gender IN('m','f')),
    ocp_bckgd_check CHAR(1) NOT NULL CHECK (ocp_bckgd_check IN('n','y')),
    ocp_notes VARCHAR(45) NULL,
    PRIMARY KEY (ocp_id),

    CONSTRAINT ux_ocp_ssn unique nonclustered (ocp_ssn ASC),
    CONSTRAINT ux_ocp_state_id unique nonclustered (ocp_state_id ASC),

    CONSTRAINT fk_occupant_applicant
    FOREIGN KEY (app_id)
    REFERENCES dbo.applicant (app_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);


IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
--note: applicant *must* provide a phone number, but can have null values
CREATE TABLE dbo.phone
(
    phn_id SMALLINT NOT NULL identity(1,1),
    app_id SMALLINT NULL, --can be null, but *must* provide at least one number
    ocp_id SMALLINT NULL,
    phn_num BIGINT NOT NULL CHECK (phn_num > 0 and phn_num <= 9999999999),
    phn_type CHAR(1) NOT NULL CHECK (phn_type IN('c','h','w','f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    --make sure combination of app_id and phn_num is unique
    CONSTRAINT ux_app_id_phn_num unique nonclustered (app_id ASC, phn_num ASC),

    --make sure combination of ocp_id and phn_num is unique
    CONSTRAINT ux_ocp_id_phn_num unique nonclustered (ocp_id ASC, phn_num ASC),

    CONSTRAINT fk_phone_applicant
      FOREIGN KEY(app_id)
      REFERENCES dbo.applicant (app_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

--ocp_id must be NO ACTION, because of the multiple paths with applicant(parent table)
--Example: if applicant delted, phone will be deleted, as will occupant
--Allowing occupant to delete will attempt to delete the same record twice--same with updating!
--ERROR! Introducing FOREIGN KEY constraint 'fk_phone_occupant' on table 'phone' may cause cycles or multiple cascade paths.
--Specify ON DELETE NO ACTION or ON UPDATE NO ACTION,
    CONSTRAINT fk_phone_occupant
      FOREIGN KEY (ocp_id)
      REFERENCES dbo.occupant (ocp-id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

--table dbo.room_type
IF OBJECT_ID (N'dbo.room_type',N'U') IS NOT NULL
DROP TABLE dbo.room_type;

CREATE TABLE dbo.room_type
(
    rtp_id TINYINT NOT NULL identity(1,1),
    rtp_name VARCHAR(45) NOT NULL,
    rtp_notes VARCHAR(255) NULL,
    PRIMARY KEY (rtp_id)
);

IF OBJECT_ID (N'dbo.room', N'U') IS NOT NULL
DROP TABLE dbo.room;

CREATE TABLE dbo.room
(
    rom_id SMALLINT NOT NULL identity(1,1),
    prp_id SMALLINT NOT NULL,
    rtp_id TINYINT NOT NULL,
    rom_size VARCHAR(45) NOT NULL,
    rom_notes VARCHAR(255) NULL,
    PRIMARY KEY (rom_id),
    
    CONSTRAINT fk_room_property
    FOREIGN KEY (prp_id)
    REFERENCES dbo.property (prp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_room_roomtype
    FOREIGN KEY (rtp_id)
    REFERENCES dbo.room_type (rtp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

SELECT * FROM information_schema.tables;

EXEC sp_msforeachtable "ALTER table ? NOCHECK CONSTRAINT all"

--DATA AND COMMENTS
--Data for table dbo.feature
INSERT INTO dbo.feature
(ftr_type, ftr_notes)

values
('Central A/C',NULL),
('Pool', NULL),
('Close to school', NULL),
('Furnished', NULL),
('Cable', NULL),
('Washer/Dryer', NULL),
('Refrigerator', NULL),
('Microwave', NULL),
('Oven', NULL),
('1-Car garage', NULL),
('2-Car garage', NULL),
('Sprinkler System', NULL),
('Security', NULL),
('Wi-Fi', NULL),
('Storage', NULL),
('Fireplace',NULL);

--Data for table dbo.room_type
INSERT INTO dbo.room_type
(rtp_name, rtp_notes)

values
('Bed',NULL),
('Bath',NULL),
('Kitchen',NULL),
('Lanai',NULL),
('Dining',NULL),
('Living',NULL),
('Basement',NULL),
('Office',NULL);

--Data for table dbo.prop_feature
INSERT INTO dbo.prop_feature
(prp_id, ftr_id, pft_notes)

values
(1,4,NULL),
(2,5,NULL),
(3,3,NULL),
(4,2,NULL),
(5,1,NULL),
(1,1,NULL),
(1,5,NULL);

--Data for table dbo.room (escape single quotation mrk by doubling single quotation mark)
INSERT INTO dbo.room
(prp_id, rtp_id, rom_size, rom_notes)

values
(1,1, '10" x 10"',NULL),
(3,2, '20" x 15"',NULL),
(4,3, '8" x 8"',NULL),
(5,4, '50" x 50"',NULL),
(2,5, '30" x 30"',NULL);

--Data for table dbo.property (will work with zip code entered as string values)
INSERT INTO dbo.property
(prp_street, prp_city, prp_state, prp_zip, prp_type, prp_rental_rate, prp_status, prp_notes)

values
('5133 3rd Road', 'Lake Worth','FL','334671234', 'house', 1800.00, 'u', NULL),
('5000 Road Road', 'Lake Value','FL','334001234', 'house', 1900.00, 'a', NULL),
('6500 Tree Road', 'Tallahassee','FL','355671200', 'apt', 700.00, 'u', NULL),
('5450 Apple Drive', 'Sarasota','FL','330101255', 'condo', 2800.00, 'a', NULL),
('4500 Windsong Lane', 'Fort Myers','FL','349471554', 'townhouse', 2000.00, 'u', NULL);

INSERT INTO dbo.applicant
(app_ssn, app_state_id, app_fname,app_lname, app_street, app_city, app_state, app_zip, app_email, app_dob, app_gender, app_bckgd_check, app_notes)

Values
('123456789','A12C34S56Q78','Carla','Vanderbilt', '5133 3rd Road', 'Lake Worth','FL','334671234','csweeney@yahoo.com','1961-11-26','F','y',NULL),
('202123303','G9B900Z20011','Howard','Mandell', '6500 4th Sreet', 'New York City','NY','334671234','hmandelly@gmail.com','1975-12-26','M','y',NULL),
('123404999','B20Z55R99Q00','CeeLo','Green', '5133 Apple Road', 'Tallahassee','FL','334500909','ceelo@yahoo.com','1980-12-02','M','y',NULL),
('365214986','dgfgr56597224','Chris','Thrombough','987 Learning Drive','Tallahassee','FL','323011234','landbeck@fsu.edu','1969-07-25','M','y',NULL),
('326598236','yadayada4517','Spencer','Moore','787 Tharpe Road','Tallahassee','FL','323061234','spencer@my.fsu.edu','1990-08-14','M','n',NULL);

INSERT INTO dbo.agreement
(prp_id, app_id,agr_signed,agr_start,agr_end,agr_amt,agr_notes)

Values
(3,4,'2011-12-01','2012-01-01','2012-12-31',1000.00,NULL),
(1,1,'1983-01-01','1983-01-01','1987-12-31',800.00,NULL),
(4,2,'1999-12-31','2000-01-01','2004-12-31',1200.00,NULL),
(5,3,'1999-07-31','1999-08-01','2004-07-31',750.00,NULL),
(2,5,'2011-01-01','2011-01-01','2013-12-31',900.00,NULL);

INSERT INTO dbo.occupant
(app_id, ocp_ssn, ocp_state_id, ocp_fname, ocp_lname, ocp_email, ocp_dob, ocp_gender, ocp_bckgd_check, ocp_notes)

VALUES
(1,'326532165','okd557ig4125','Bridget','Case-Sweeney','bcs10c@gmail.com','1988-03-23','F','y',NULL),
(1,'187452457','uhtooold','Brian','Sweeney','brian@sweeney.com','1956-07-28','M','y',NULL),
(2,'123456780','thisisdoggie','Skittles','McGoobs','skittles@wags.com','2011-01-01','F','n',NULL),
(2,'098123664','thisiskitty','Smalls','Balls','smalls@meanie.com','1988-03-05','M','n',NULL),
(5,'857694032','thisisbaby324','Baby','Girl','baby@girl.com','2013-04-08','F','n',NULL);

INSERT INTO dbo.phone
(app_id, ocp_id, phn_num, phn_type, phn_notes)

VALUES
(1,NULL,'5615233044','H',NULL),
(2,NULL,'5615689976','F',NULL),
(5,5,'8504569872','H',NULL),
(NULL,1,'5613080898','C','occupant"s number only'),
(3,NULL,'8504152365','W',NULL);

exec sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"