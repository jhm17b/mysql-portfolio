--Project 1 SQL statements

--1. List all financial institutions, full user names, account types and the date they were opened, only
--include checking accounts, group by insistution name in ascending order and limit to two records displayed.

--natural join
select ins_name, usr_fname, usr_lname, act_type, src_start_date
from insistution
  natural join source
  natural join user
  natural join account
where act_type="checking"
group by ins_id, usr_id, src_id
order by ins_namelimit 0,2;

--join using
select ins_name, usr_fname, usr_lname, act_type, src_start_date
from insistution
  join source using(ins_id)
  join user using(usr_id)
  join account using(act_id)
where act_type="checking"
group by ins_id, usr_fname, usr_lname, src_start_date
order by ins_name
limit 0,2;

