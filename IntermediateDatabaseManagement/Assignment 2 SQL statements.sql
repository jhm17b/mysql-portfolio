/*Show databases; */
use jhm17b; --Must use this command to do something in databases
show tables;

/*drop database if exists jhm17b;
create database if not exists jhm17b;
use jhm17b;
show tables;

--Number 1. List all faculty members' first and last names, full address, salaries, and hire dates.
--All of these joins are correct

--old style join*/
select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from Person p
  join employee e on p.per_id=e.per_id
  join faculty f on e.per_id=f.per_id;

--join on
select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from Person p
  join employee e on p.per_id=e.per_id
  join faculty f on e.per_id=f_per_id;

--join using
select per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from Person
  join employee using (per_id)
  join faculty using (per id);

--natural join
select per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from Person
  natural join employee
  natural join faculty;  

  Query results:
  mysql> select per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
    -> from Person
    ->   natural join employee
    ->   natural join faculty;
+--------+-----------+------------+-------------------------------------+------------------+-----------+------------+----------------+
| per_id | per_fname | per_lname  | per_street                          | per_city         | per_state | emp_salary | fac_start_date |
+--------+-----------+------------+-------------------------------------+------------------+-----------+------------+----------------+
|      1 | Joseph    | Myers      | P.O. Box 356, 3450 Sodales Ave      | Cincinnati       | OH        |   44566.50 | 2003-07-01     |
|      2 | Len       | Barnes     | P.O. Box 708, 5157 Arcu Ave         | Helena           | MT        |   33546.74 | 2019-08-08     |
|      3 | Tyrone    | Walker     | P.O. Box 574, 8973 Arcu. St.        | Provo            | UT        |   85252.66 | 2015-05-04     |
|      4 | Deacon    | Berger     | P.O. Box 779, 4474 Cras St.         | Boston           | MA        |   44679.99 | 2010-03-01     |
|      5 | Reed      | Camacho    | 9020 Nibh. St.                      | South Burlington | VT        |   76502.72 | 1979-12-07     |
|      6 | Maxine    | Parrish    | P.O. Box 936, 7583 At, Road         | Eugene           | OR        |   55214.44 | 2013-12-01     |
|      7 | Penelope  | Byers      | Ap #883-5297 Phasellus Ave          | Savannah         | GA        |   75643.59 | 2011-09-09     |
|      8 | Inga      | Fitzgerald | Ap #302-7690 Donec Road             | Lawton           | OK        |   54555.66 | 2010-10-11     |
|      9 | Adrian    | Leon       | 620-2122 Nunc Ave                   | Hartford         | CT        |   29777.09 | 1999-09-10     |
|     10 | Lysandra  | Gordon     | P.O. Box 240, 2320 Fermentum Street | Metairie         | LA        |   99754.11 | 1980-08-01     |
+--------+-----------+------------+-------------------------------------+------------------+-----------+------------+----------------+
10 rows in set (0.00 sec)



--Number 2. List the first 10 alumni's names, genders, DOBs, degree types, areas, and dates
--old style join
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from Person p, alumnus a, degree d
where p.per_id=a.per_id
 and a.per_id=d.per_id
limit 0,10; --Might not work

--join on

--join using


--natural join
select per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from Person
  natural join alumnus
 natural join degree
limit 0,10;  
--Query Result set
mysql> select per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
    -> from Person
    ->   natural join alumnus
    ->  natural join degree
    -> limit 0,10;
+--------+-----------+-----------+------------+------------+----------------------+------------------------+------------+
| per_id | per_fname | per_lname | per_gender | per_dob    | deg_type             | deg_area               | deg_date   |
+--------+-----------+-----------+------------+------------+----------------------+------------------------+------------+
|      1 | Joseph    | Myers     | m          | 2019-09-18 | Associate of Science | Information Technology | 2000-12-22 |
|      2 | Len       | Barnes    | f          | 2019-06-30 | Associate of Science | Biology                | 2010-05-10 |
|      3 | Tyrone    | Walker    | m          | 2020-08-27 | Bachelor of Science  | Mathematics            | 2019-05-25 |
|      4 | Deacon    | Berger    | m          | 2020-08-23 | Bachelor of Arts     | Theatre                | 2018-12-20 |
|      5 | Reed      | Camacho   | m          | 2019-06-09 | Master of Science    | Information Technology | 2019-12-20 |
+--------+-----------+-----------+------------+------------+----------------------+------------------------+------------+
5 rows in set (0.00 sec)



--Number 3. List the last 20 undergrad names, majors, tests, scores, and standings.
--Natural join
select per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
from Person
  natural join student
 natural join undergrad
order by per_id desc
limit 0,20;

--Query Result Set
mysql> select per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
    -> from Person
    ->   natural join student
    ->  natural join undergrad
    -> order by per_id desc
    -> limit 0,20;
+--------+-----------+------------+------------------------+----------+-----------+--------------+
| per_id | per_fname | per_lname  | stu_major              | ugd_test | ugd_score | ugd_standing |
+--------+-----------+------------+------------------------+----------+-----------+--------------+
|     35 | Brynn     | Decker     | History                | sat      |      1200 | Senior       |
|     34 | Daryl     | Buckner    | Art                    | sat      |      1080 | Junior       |
|     33 | Otto      | Stephenson | Computer Science       | act      |        22 | Freshman     |
|     32 | Jana      | Le         | Theatre                | act      |        20 | Sophomore    |
|     31 | Hanna     | Foreman    | Information Technology | sat      |      1140 | Freshman     |
+--------+-----------+------------+------------------------+----------+-----------+--------------+
5 rows in set (0.00 sec)

--Number 4. Remove the first 10 staff members; after which, display the remaining staff members' names
--and positions
select *from staff;

--delete the first 3 staff members