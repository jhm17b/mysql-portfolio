/*Show databases; */
use jhm17b; -- Must use this command to do something in databases
show tables;

/*drop database if exists jhm17b;
create database if not exists jhm17b;
use jhm17b;
show tables;
source db/premiere.sql



/*Assignment 1 Questions and statements.*/
/*Number 1 List all the contents (rows or records) of the ORDERS table */
select *from orders;



/*Number 2 Create an alias for an attribute name*/
select slsrep_number as slsrep_number
from sales_rep
where slsrep_number='06';



/*Number 3 List the order line number, part number, number ordered, and quoted price
from the ORDER_LINE*/
select order_number, part_number, number_ordered, quoted_price
from order_line
order by quoted_price asc;



/*Number 4 Remove part number CB03 from the PART table */
select *from part; -- select before delete

delete
from part
where part_number='cb03';

select *from part; -- select after delete to check



/*Number 5 Modify the city, state, and zip of sales rep number 6
Verify with select command */
select *from sales_rep; -- select before update

UPDATE sales_rep
SET city='Tallahassee', state='FL', zip_code='32304'
WHERE slsrep_number='06';

select *from sales_rep; -- select after update



/*Number 6 Add TWO records to the parttable. */
select *from part; -- select before insert

INSERT INTO part
(part_number, part_description, units_on_hand, item_class, warehouse_number, unit_price)
VALUES
('yyy','Widget1','5','SS',1,9.95),
('zzz','Widget2','10','TT',2,10.95);

select *from part; -- select after insert



/*Number 7 (ON gmc.sql) List all dealership names, vehicle types and makes for each dealership
(use EQUI-JOIN, aka "old-style" join) */
select dlr_name, veh_type, veh_make
from dealership, vehicle
where dealership.dlr_id = vehicle.dlr_id;

-- OR
select dlr_name, veh_type, veh_make
from dealership as

 d, vehicle as v
where d.dlr_id = v.dlr_id;



/* Number 8 List all dealership names, as well as all sales reps first,
last names, and their total sales for each dealership (use JOIN ON) */
select dlr_name, srp_fname, srp_lname, srp_tot_sales
from dealership
join slsrep on dealership.dlr_id = slsrep.dlr_id;



/* Number 9 list how many vehicles each dealership owns (display dealer id,
name, and *number* of vehciles for each dealership) */
select dlr_id, dlr_name, count(veh_type)
from dealership
 join vehicle using (dlr_id)
group by dlr_id;



/* Number 10 List each dealership's total sales, include dealer's name
and total sales (captured in dealership_history table), use NATURAL JOIN*/
select dlr_name, sum(dhs_ytd_sales) as total_sales
from dealership
 natural join dealership_history
group by dlr_id; 